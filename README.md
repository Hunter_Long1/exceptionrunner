Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?
Since output to logger.log is managed by a centralized logging service, it provides additional debugging info that is not provided by default in addition to a filter for different levels of debugging. The console filters out everything but the INFO level.
1.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
This comes from the logger within JUnit.
1.  What does Assertions.assertThrows do?
Assertions.assertThrows verifies whether or not the block of code throws the proper exception.
1.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)
    serialVersionUID is used to verify that a serialized class matches the receiver upon deserialization. A default one is provided but it is recommended to provide a custom one.  
    2.  Why do we need to override constructors?
    Unlike methods and other fields, constructors are not "inherited" in the same fashion. The derived class will not have any of its own constructors to handle the necessary functionality.
    3.  Why we did not override other Exception methods?
    Similar to the reasoning in the above question; methods are inherited to the derived class and thus do not need to be implemented for the derived class.
1.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)
This static block is called once for every instance of Timer initializing the configuration of the Logger instance used across every timer beforehand.
1.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
README.md uses the Markdown syntax. BitBucket uses Markdown to format text for both README files and for descriptions & comments of pull requests.
1.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)
A NullPointerException is thrown inside of the finally block of the timeMe function instead of the expected TimerException. timeNow is being used in the finally block, however, timeNow is not guaranteed to not be null if a TimerException is thrown. We need to verify that timeNow is, in fact, not null if we are going to use it.
1.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)
Although a TimerException would be thrown, the finally block of the timeMe method has to be executed before bubbling up the call stack. And since, in this case, timeNow is null, another exception is thrown while still in the finally block of timeMe which is NOT caught.
1.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 
1.  Make a printScreen of your eclipse Maven test run, with console
1.  What category of Exceptions is TimerException and what is NullPointerException
A NullPointerException is of the category "unchecked" whereas our TimerException is of the category "checked."
1.  Push the updated/fixed source code to your own repository.